import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootObject, Issue } from 'src/model/json-object';
import { SettingsService } from './settings.service';
import { createAttribute } from '@angular/compiler/src/core';
import { Day } from 'src/model/day';
import { Card } from 'src/model/card';
import { Row } from 'src/model/row';


@Injectable({
  providedIn: 'root'
})
export class BoardService {
  firstCard: Card;
  lastCard: Card;
  firstCardStartDate: Date;
  days: Day[] = [];
  cards: Card[] = [];
  rows: Row[] = [];



  constructor(
    private http: HttpClient,
    private settingsService: SettingsService
  ) { }

  async getSampleData() {

    await this.http
      .get('assets/json/sample_data.json')
      .toPromise()
      .then(res => {
        this.cards = [];
        this.days = [];
        let temp = res as RootObject;

        this.mapRootObjectToCards(temp)

        this.caculateFirstCardStartDate();

        this.generateTimeline();

        this.repairLeftAndDueDate();

        this.cards.sort((a, b) => (a.left < b.left ? -1 : 1));
      });

  }

  private mapRootObjectToCards(temp: RootObject) {
    temp.issues.forEach(issue => {

      if (issue.fields.duedate !== null) {

        //#region initiate newCard
        const newCard: Card = {
          dueDate: new Date(issue.fields.duedate),
          dueDateInSeconds: 0,
          estimateDays: 0,
          estimateSeconds: 0,
          key: issue.key,
          summary: issue.fields.summary,
          description: issue.fields.description,
          labels: issue.fields?.labels,
          estimationText: issue.fields.timetracking.originalEstimate,
          dueDateText: issue.renderedFields.duedate,
          left: 0,
          visible: false,
          width: 0,
          issuetype: issue.fields?.issuetype,
          creator: issue.fields?.creator
        };
        //#endregion

        //#region calculate dueDate and dueDateInSeconds
        newCard.dueDate.setHours(23);
        newCard.dueDate.setMinutes(59);
        newCard.dueDate.setSeconds(59);
        newCard.dueDateInSeconds = newCard.dueDate.getTime() / 1000;
        //#endregion

        //#region calculate estimateDays and estimateSeconds(in 24 hours format)
        if (issue.fields.timetracking.originalEstimate?.endsWith('d') || issue.fields.timetracking.originalEstimate?.endsWith('w')) {
          newCard.estimateSeconds = issue.fields.timetracking.originalEstimateSeconds * 3;
        }
        else if (issue.fields.timetracking.originalEstimate?.endsWith('h')) {
          newCard.estimateSeconds = 24 * 3600;
        }
        newCard.estimateDays = newCard.estimateSeconds / (24 * 3600);
        //#endregion

        //#region calculate left and width of card (left will be changed later)
        newCard.left = newCard.dueDateInSeconds - newCard.estimateSeconds;

        newCard.width = newCard.estimateSeconds;
        //#endregion

        //#region detect firstCard and lastCard
        if (this.firstCard === undefined || newCard.left < this.firstCard.left) {
          this.firstCard = newCard;
        }
        if (this.lastCard == undefined || this.lastCard.dueDateInSeconds < newCard.dueDateInSeconds) {
          this.lastCard = newCard;
        }
        //#endregion

        newCard.visible = true;

        this.cards.push(newCard);

        this.generateNewRow(issue);
      }

    });
  }

  private caculateFirstCardStartDate() {
    this.firstCardStartDate = new Date(this.firstCard.dueDate);
    let firstCardEstimateDays = this.firstCard.estimateSeconds / (24 * 3600);

    let i = 0;
    while (firstCardEstimateDays > 0) {
      if (i > 0) {
        this.firstCardStartDate.setDate(this.firstCardStartDate.getDate() - 1);
      }

      //ignore saturday and sunday
      if ([0, 6].indexOf(this.firstCardStartDate.getDay()) === -1) {
        firstCardEstimateDays--;
      }
      else if (i === 0) { //when due date is saturday or sunday, we move it to 1 or 2 days after
        this.firstCardStartDate.setDate(this.firstCardStartDate.getDate() + 1);
        continue;
      }
      i++;
    }
  }

  private generateTimeline() {

    let tempFirstCardStartDate = new Date(this.firstCardStartDate);

    //#region add 2 days gap
    let gapBeforeStart = 2;
    while (gapBeforeStart > 0) {
      tempFirstCardStartDate.setDate(tempFirstCardStartDate.getDate() - 1);
      if ([0, 6].indexOf(tempFirstCardStartDate.getDay()) === -1) {
        gapBeforeStart--;
      }
    }
    //#endregion

    let totalDays = (this.lastCard.dueDate.getTime() - tempFirstCardStartDate.getTime()) / 1000 / 60 / 60 / 24;

    totalDays = Math.ceil(totalDays) + 2; //when due date is saturday or sunday, we move it to 1 or 2 days after

    //#region calculate totalDays without weekend
    let dayCounter = 0;
    for (let i = 0; i < totalDays; i++) {
      if (i > 0)
        tempFirstCardStartDate.setDate(tempFirstCardStartDate.getDate() + 1);
      if ([0, 6].indexOf(tempFirstCardStartDate.getDay()) === -1) {
        let newDay: Day = {
          date: new Date(tempFirstCardStartDate),
          text: tempFirstCardStartDate.toDateString(),
          left: dayCounter * 86400
        }
        dayCounter++;
        this.days.push(newDay);
      }
    }
    this.settingsService.totalDays = dayCounter;
    //#endregion

  }

  private generateNewRow(issue: Issue) {
    for (let i = 0; i < issue.fields?.labels?.length; i++) {
      if (this.rows.findIndex(x => x.title === issue.fields.labels[i]) === -1) {
        const newRow: Row = {
          title: issue.fields.labels[i]
        }
        if (newRow.title.toLowerCase() === 'roadmap')
          this.rows.unshift(newRow);
        else
          this.rows.push(newRow);
      }
    }

  }

  private repairLeftAndDueDate() {
    this.cards.forEach(card => {

      let index = -1;
      while (index === -1) {

        index = this.days.findIndex(x => x.text == card.dueDate.toDateString())

        if (index !== -1) {
          card.left = this.days[index - card.estimateDays + 1].left;
        }
        else { //when due date is saturday or sunday, we move it to 1 or 2 days after
          card.dueDate.setDate(card.dueDate.getDate() + 1)
        }

      }
    });
  }
}
