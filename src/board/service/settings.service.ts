import { Injectable, ElementRef } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class SettingsService {

  isDragging: boolean = false;

  secondToPixelRatio = 0.0025;
  changeRatioValue = 0.0005;
  minRatioValue = 0.001;

  totalDays = 0;

  constructor() {

  }



}
