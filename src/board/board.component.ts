import { Component, OnInit } from '@angular/core';
import { RootObject, Issue } from 'src/model/json-object';
import { Day } from 'src/model/day';
import { BoardService } from './service/board.service';
import { SettingsService } from './service/settings.service';
import { Card } from 'src/model/card';
import { Row } from 'src/model/row';
import { Filter } from 'src/model/filter';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
  providers: [BoardService]

})
export class BoardComponent implements OnInit {

  cards: Card[] = [];
  days: Day[] = [];
  rows: Row[] = [];

  filteredRows: Row[] = [];
  filteredCards: Card[] = [];

  filters: Filter = {
    issueType: 'Epic',
    rowLabel: ''
  }

  constructor(private boardService: BoardService, public settingsService: SettingsService) { }

  async ngOnInit() {
    await this.getData();
  }

  async getData() {
    this.cards = [];
    this.days = [];
    this.rows = [];
    this.filteredRows = [];
    this.filteredCards = [];

    await this.boardService.getSampleData();

    this.cards = this.boardService.cards;
    this.days = this.boardService.days;
    this.rows = this.boardService.rows;
    this.filterData();

  }

  filterData() {
    this.filteredRows = this.rows.filter(x => x.title.toLowerCase().indexOf(this.filters.rowLabel.toLowerCase()) !== -1);
    this.filteredCards = this.cards.filter(x => x.issuetype?.name.toLowerCase() === this.filters.issueType.toLowerCase());
  }

  zoom(value: number) {
    this.settingsService.secondToPixelRatio += value;
  }



}
