import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BoardRoutingModule } from './board-routing.module';
import { BoardComponent } from './board.component';
import { RowComponent } from './components/row/row.component';
import { CardComponent } from './components/card/card.component';
import { BoardService } from './service/board.service';
import { MatDialogModule } from '@angular/material/dialog';
import { CardDetailsComponent } from './components/card-details/card-details.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    BoardComponent,
    RowComponent,
    CardComponent,
    CardDetailsComponent
  ],
  imports: [
    CommonModule,
    BoardRoutingModule,
    MatDialogModule,
    MatTooltipModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    DragDropModule,
    MatIconModule,
    MatSnackBarModule,
    MatButtonModule
  ],
  exports: [
  ],
  providers: [

  ]
})
export class BoardModule { }
