import { Component, Input, OnInit } from '@angular/core';
import { SettingsService } from 'src/board/service/settings.service';
import { Card } from 'src/model/card';
import { Issue } from 'src/model/json-object';
import { Row } from 'src/model/row';

@Component({
  selector: 'app-row',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss']
})
export class RowComponent implements OnInit {

  @Input() height = '35vh';
  @Input() cards: Card[];
  @Input() index: number;
  @Input() row: Row;

  borderClass = 'border-class';

  constructor(public settingsService: SettingsService) { }

  ngOnInit(): void {
    this.borderClass += this.index % 5;

  }

}
