import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit } from '@angular/core';
import { SettingsService } from 'src/board/service/settings.service';
import { Card } from 'src/model/card';
import { RootObject, Issue } from '../../../model/json-object'
import { MatDialog } from '@angular/material/dialog';
import { CardDetailsComponent } from '../card-details/card-details.component';
import { Row } from 'src/model/row';
import { CardDetailsDialog } from 'src/model/card-details-dialog';
import { BoardService } from 'src/board/service/board.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {


  @Input() card: Card;
  @Input() class: string = '';
  @Input() index: number;
  @Input() row: Row;

  bgClass = 'bg-class';
  top = '';

  constructor(public settingsService: SettingsService, private dialog: MatDialog, private boardService: BoardService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.bgClass += this.index % 5;
    const rnd = Math.floor(Math.random() * 8) + -8;
    if (this.index % 3 === 0)
      this.top = (8 + Math.abs(rnd)).toString() + 'px';
    else
      this.top = ((this.index % 3) * 30) + rnd + '%'
  }

  openDialog() {
    const tempData: CardDetailsDialog = {
      card: this.card,
      row: this.row,
    }
    this.dialog.open(CardDetailsComponent, {
      data: tempData,
      hasBackdrop: true,
      maxWidth: '600px'
    });

  }


  dropped(event: CdkDragEnd) {
    let left = event.source.getFreeDragPosition().x / this.settingsService.secondToPixelRatio + this.card.left;
    let index = this.boardService.days.findIndex(x => x.left > left);
    event.source._dragRef.reset();
    this.card.left = this.boardService.days[index - 1].left;
    this.snackBar.open(`New start date: ${this.boardService.days[index - 1].text}`, undefined, {
      duration: 2000,
    })
  }
}
