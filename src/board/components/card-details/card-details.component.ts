import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Card } from 'src/model/card';
import { CardDetailsDialog } from 'src/model/card-details-dialog';
import { Row } from 'src/model/row';


@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})
export class CardDetailsComponent implements OnInit {

  card: Card;
  row: Row;

  constructor(private dialogRef: MatDialogRef<CardDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CardDetailsDialog) {
    this.card = data.card;
    this.row = data.row;
  }

  ngOnInit(): void {
  }

}
