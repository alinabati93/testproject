import { Creator, Issuetype, Timetracking } from "./json-object";

export interface Card {
  key: string;
  summary: string;
  description?: string;
  visible: boolean;
  left: number;
  width: number;
  dueDate: Date;
  dueDateInSeconds: number;
  estimateDays: number;
  estimateSeconds: number;
  estimationText?: string;
  dueDateText?: string;
  issuetype?: Issuetype;
  labels: string[];
  timetracking?: Timetracking;
  creator: Creator;
}
