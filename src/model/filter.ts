export interface Filter {
  rowLabel: string;
  issueType: string;
}
