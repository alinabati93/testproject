export interface Day {
  date: Date,
  text: string,
  left: number
}
