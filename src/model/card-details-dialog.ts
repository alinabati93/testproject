import { Card } from "./card";
import { Row } from "./row";

export interface CardDetailsDialog {
  card: Card;
  row: Row;
}
